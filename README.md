Project 2: Make a Contact List

In this project, you will create a Web page on making a contact list by applying HTML, CSS and
JavaScript. Specifically,

1.   Use all the letters in the Alphabet (i.e., A - Z).
2.   Create alphabet nav bar to direct the specific letter accordingly.
3.   List 15 people in the list with the format “Last name, First name”.
4.   Make a button for adding people (name, email, phone number).
5.   When you click the people’s name, it needs to show the email and phone number on the same page.
6.   Make the even line and the odd line of the list in different colors.

Please validate your HTML code and CSS code before submit, through the following the validating website: https://validator.w3.org/#validate_by_input+with_options

Use the Developer Tools for debugging your JavaScript code.

Here are some screenshots of the sample page, which indicates the expectation of this project. 

The beginning page:

![image002.jpg](https://bitbucket.org/repo/4dKq8y/images/2481724286-image002.jpg)

When you click “add one people”, the page will pop up four dialogs to let a user entering First
Name, Last Name, Email, and Phone number.

![image004.jpg](https://bitbucket.org/repo/4dKq8y/images/1617549664-image004.jpg)

 
 
When typing the name, please be careful with the “unhappy” cases. For example:

1.   “ First Name” or “ Last Name”. That is, there is a space before input first name or last name.
2.   Duplicate name. If a user input the name that is already in the list, a message will be shown to indicate the duplicate name, and this duplicate name will be ignored.

After you input some contact information into the list, your list will be shown as following:

![image006.jpg](https://bitbucket.org/repo/4dKq8y/images/3498918300-image006.jpg)
 

Note that in the list, even lines and odd lines are showing different colors.

When you click the person’s name, your page will have a block to show the detail information of this person, as shown in the above screenshot.



Please email the TA or the instructor when you have a question.