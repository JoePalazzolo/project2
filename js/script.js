var contacts = [];
var indexHeadings = [];

function initialize() {
	// createContacts();
	refreshPage();
}

function refreshPage() {
	sortList();
	getHeadings();
	createIndex();
	createContactList();			
}

function addName() {
	var firstname = prompt("First Name").trim();
	var lastname = prompt("Last Name").trim();
	var mail = prompt("Mail");
	var phone = prompt("Phone");
	var duplicate = false;
	for(var i = 0; i < contacts.length; i++) {
		if ((firstname == contacts[i].firstname) && (lastname == contacts[i].lastname)) {
			duplicate = true;
			break;
		}
	}
	if (duplicate == true) {
		alert("You have entered a name that is already in the contact list.  Try again.");
	} else {
		contacts.push({firstname: firstname, lastname: lastname, mail: mail, phone: phone});
		refreshPage();
	}
}

function createContacts() {
	contacts = [
		{
			firstname: "Joe",
			lastname: "Palazzolo",
			mail: "palajm01@students.ipfw.edu",
			phone: "260-555-1234"
		}, {
			firstname: "Joe",
			lastname: "Bucko",
			mail: "rihmkllr@fac.gov",
			phone: "260-555-1234"
		}, {
			firstname: "Will",
			lastname: "Bucko",
			mail: "rihmkllr@fac.gov",
			phone: "260-555-1234"
		}, {
			firstname: "Adam",
			lastname: "Bucko",
			mail: "rihmkllr@fac.gov",
			phone: "260-555-1234"
		}, {
			firstname: "Ebn",
			lastname: "Ozn",
			mail: "aeiou@sometimesy.com",
			phone: "260-555-1234"
		}, {
			firstname: "Joshua",
			lastname: "Chamberlain",
			mail: "lawrence@20maine.com",
			phone: "260-555-1234"
		}, {
			firstname: "Sam",
			lastname: "Grant",
			mail: "no1general@unionarmy.mil",
			phone: "260-555-1234"
		}, {
			firstname: "Arthur",
			lastname: "Wellesley",
			mail: "nappybeater@ironduke.uk",
			phone: "260-555-1234"
		}, {
			firstname: "Robert",
			lastname: "Heinlein",
			mail: "knitter@tertius.plt",
			phone: "260-555-1234"
		}, {
			firstname: "Johnny",
			lastname: "Rico",
			mail: "bughunter@rodgeryoung.sst",
			phone: "260-555-1234"
		}, {
			firstname: "Horatio",
			lastname: "Hornblower",
			mail: "horatio@britishnavy.mil",
			phone: "260-555-1234"
		}, {
			firstname: "Misty",
			lastname: "Chincoteague",
			mail: "pony@wcdas.gov",
			phone: "260-555-1234"
		}, {
			firstname: "Tom",
			lastname: "Clancy",
			mail: "tclancy@redoctober.ru",
			phone: "260-555-1234"
		}, {
			firstname: "Stephen",
			lastname: "King",
			mail: "pennywise@bangor.com",
			phone: "260-555-1234"
		}, {
			firstname: "Richard",
			lastname: "Winters",
			mail: "dickw@101st.mil",
			phone: "260-555-1234"
		}, {
			firstname: "Zesheng",
			lastname: "Chen",
			mail: "chenz@ipfw.edu",
			phone: "260-555-1234"
		}, {
			firstname: "Dingding",
			lastname: "Diao",
			mail: "diaod01@students.ipfw.edu",
			phone: "260-555-1234"
		}
	]
}

function sortList() {
	contacts.sort(function(a, b) {
		if(a.lastname < b.lastname) return -1;
		if(a.lastname > b.lastname) return 1;
		if(a.firstname < b.firstname) return -1;
		if(a.firstname > b.firstname) return 1;
		return 0;
	});
}

function getHeadings() {
	var lastHeading = "";
	indexHeadings = [];
	for (var i = 0; i < contacts.length; i++) {
		var currentHeading = contacts[i].lastname.charAt(0);
		if (currentHeading != lastHeading) {
			indexHeadings.push(currentHeading);
			lastHeading = currentHeading;
		}
	}
}

function isInArray(value, array) {
	return array.indexOf(value) > -1;
}

function createIndex() {
	var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var indexHTML = "";
	for (var i = 0; i < alphabet.length; i++) {
		var letter = alphabet.substr(i,1);
		if (isInArray(letter, indexHeadings)){
			indexHTML = indexHTML.concat("<a href=\"#index", letter, "\" class=\"alpha\">", letter,  "</a> ");
		} else {
			indexHTML = indexHTML.concat(" ", letter, " ");
		}
	}
	document.getElementById('index').innerHTML = indexHTML;
}

function createContactList() {
	var headerLetter = 0;
	var indexCount = 0;
	var contactCount = 0;
	var currentHeading ="";
	var alternateContact;
	var alternateClass;
	var contactsHTML = "<table>\n";
	for (var i = 0; i < contacts.length; i++) {
		currentHeading = contacts[i].lastname.charAt(0);
		if (indexHeadings[indexCount] == currentHeading) {
			contactsHTML = contactsHTML.concat("<tr class=spacer><td></td><td></td></tr>");
			contactsHTML = contactsHTML.concat("<tr><th colspan=\"2\"><a name=\"index",currentHeading, "\">", currentHeading, "</a></th></tr>");
			contactsHTML = contactsHTML.concat("<tr class=spacer><td></td><td></td></tr>");
				indexCount++;
				alternateContact = 0;
		}
		if (isOdd(alternateContact)) {
			alternateClass = " class=\"alternate\"";
		} else {
			alternateClass = "";
		}
		var contactName = getContactName(i);
		contactsHTML = contactsHTML.concat("<tr><td class=blank></td><td onclick=\"showContact('", contactName, "')\"", alternateClass, ">", contactName, "</td></tr>");
			contactCount++;
			alternateContact++
	}
	document.getElementById('contactlist').innerHTML = contactsHTML;
}

function isOdd(num) {
	return num % 2;
}

function getContactName(contactCount) {
	return contacts[contactCount].firstname + " " + contacts[contactCount].lastname;
}

function showContact(contactName) {
	var contactHTML = "name: " + contactName + getMailPhone(contactName);
	document.getElementById('selectedcontact').innerHTML = contactHTML;
	document.body.scrollTop = document.documentElement.scrollTop = 0;
}

function getMailPhone(contactName) {
	var mailPhoneHTML = "";

	for (var i = 0; i < contacts.length; i++) {
		if (contactName == getContactName(i)) {
	    	mailPhoneHTML = "<br />mail: " + contacts[i].mail + "<br />phone: " + contacts[i].phone;
	    	break;
 	   }
	}
	return mailPhoneHTML;
}

